import { App } from './app';

export const APPS:App[] = [
    {
        app_type:"endline_qc",
        factory:"sublime"
    },
    {
        app_type:"line_in",
        factory:"sublime"
    },
    {
        app_type:"packing_in",
        factory:"sublime"
    },
    {
        app_type:"endline_qc",
        factory:"ananta"
    },
    {
        app_type:"sew_inline",
        factory:"ananta"
    },
    {
        app_type:"line_in",
        factory:"ananta"
    },
    {
        app_type:"line_in2",
        factory:"ananta"
    },
];