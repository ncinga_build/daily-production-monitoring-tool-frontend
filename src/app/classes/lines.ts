import { Line } from './line';

export const LINES: Line[] = [
    {
        factory:"sublime",
        name:"sgt-sublime-floor3-sewing-line1"
    },
    {
        factory:"sublime",
        name:"sgt-sublime-floor3-sewing-line2"
    },
    {
        factory:"sublime",
        name:"sgt-sublime-floor3-sewing-line3"
    },
    {
        factory:"sublime",
        name:"sgt-sublime-floor3-sewing-line4"
    },
    {
        factory:"sublime",
        name:"sgt-sublime-floor3-sewing-line5"
    },
    {
        factory:"sublime",
        name:"sgt-sublime-floor3-sewing-line6"
    },
    {
        factory:"ananta",
        name:"ncsant-aal-aepz-floor1-sewing-section1-line3"
    }
];