import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../services/auth.service';
import { DataService } from '../../services/data.service';

@Component({
  selector: 'app-packing-in-sublime',
  templateUrl: './packing-in-sublime.component.html',
  styleUrls: ['./packing-in-sublime.component.css']
})
export class PackingInSublimeComponent implements OnInit {

  selected_date:String;

  stations = {
    
    station1:[
      "floor3-packing-line1",
      "default",
      "K01"
    ],
    station2:[
      "floor3-packing-line2",
      "default",
      "K02"
    ],
    station3:[
      "floor3-packing-line3",
      "default",
      "K03"
    ],
    station4:[
      "floor3-packing-line4",
      "default",
      "K04"
    ],
    station5:[
      "floor3-packing-line5",
      "default",
      "K05"
    ],
    station6:[
      "floor3-packing-line6",
      "default",
      "K06"
    ]

  }

  at:String = "packing_in";

  btn_classes = {
    station1:"row panel",
    station2:"row panel",
    station3:"row panel",
    station4:"row panel",
    station5:"row panel",
    station6:"row panel"
  }

  constructor(
    private authService:AuthService,
    private dataService:DataService
  ) { }

  ngOnInit() {

    this.dataService.currentDateValue.subscribe(date => this.selected_date = date);

    for (let entry in this.stations) {

      //console.log(this.dataService.getCurrentDate());

      this.authService.getFirstEvent(this.stations[entry][0],"sublime",this.selected_date,this.stations[entry][1],this.at).subscribe(data => {

        if(data.hits.hits.length != 0){
          this.btn_classes[entry] = "green-palatte";
          // console.log(data.hits.hits);
        } else {
          this.btn_classes[entry] = "red-palatte";
          // console.log("data is not coming.......");
        }
      });
    }

  }

}
