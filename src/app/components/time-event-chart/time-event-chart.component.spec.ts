import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TimeEventChartComponent } from './time-event-chart.component';

describe('TimeEventChartComponent', () => {
  let component: TimeEventChartComponent;
  let fixture: ComponentFixture<TimeEventChartComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TimeEventChartComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TimeEventChartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
