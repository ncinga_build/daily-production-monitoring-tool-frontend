import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../services/auth.service';
import * as CanvasJS from '../../../canvasjs.min.js';

@Component({
  selector: 'app-time-event-chart',
  templateUrl: './time-event-chart.component.html',
  styleUrls: ['./time-event-chart.component.css']
})
export class TimeEventChartComponent implements OnInit {

  constructor(
    private authService:AuthService
  ) { }

  ngOnInit() {

    this.authService.getTimeEvent("sgt-sublime","2018-10-28","sgt-sublime-floor3-sewing-line6").subscribe(data => {

      var data_point_array = new Array(0);

      for (var i = 0; i < data.aggregations[2].buckets.length; i++) {

        data_point_array.push(
          {
            x:data.aggregations[2].buckets[i].key,
            y:data.aggregations[2].buckets[i][1].value
          }
        );

      }

      //console.log(data_point_array);

      let chart = new CanvasJS.Chart("chartContainer", {
      
        animationEnabled: true,
        exportEnabled: true,
        title:{
          text: "Time Event Monitoring"
        },
        axisY:{ 
          title: "Sum of points.attrib_val",
          includeZero: false, 
          interval: 10000,
          valueFormatString: "#"
        },
        axisX:{ 
          title: "time_tags.datetime per minute"
        },
        data: [{
          type: "stepLine",
          xValueType: "dateTime",
          markerSize: 1,
          dataPoints: data_point_array
        }]
      });
  
      chart.render();

    });

  }

}
