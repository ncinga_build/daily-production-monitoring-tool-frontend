import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../services/auth.service';
import { DataService } from '../../services/data.service'; 

@Component({
  selector: 'app-endline-qc',
  templateUrl: './endline-qc.component.html',
  styleUrls: ['./endline-qc.component.css']
})
export class EndlineQcComponent implements OnInit {

  selected_date:String;

  stations = {
    station1:[
      "floor1-sewing-section1-line1",
      "primary",
      "TEAM01"
    ],
    station2:[
      "floor1-sewing-section1-line1",
      "assembly_point1",
      "TEAM01"
    ],
    station3:[
      "floor1-sewing-section1-line2",
      "primary",
      "TEAM02"
    ],
    station4:[
      "floor1-sewing-section1-line3",
      "primary",
      "TEAM03"
    ],
    station5:[
      "floor1-sewing-section1-line4",
      "primary",
      "TEAM04"
    ],
    station6:[
      "floor1-sewing-section1-line2",
      "assembly_point1",
      "TEAM02"
    ],
    station7:[
      "floor1-sewing-section1-line3",
      "assembly_point1",
      "TEAM03"
    ],
    station8:[
      "floor1-sewing-section1-line4",
      "assembly_point1",
      "TEAM04"
    ],
    station9:[
      "floor1-sewing-section2-line5",
      "primary",
      "TEAM05"
    ],
    station10:[
      "floor1-sewing-section2-line5",
      "assembly_point1",
      "TEAM05"
    ],
    station11:[
      "floor1-sewing-section2-line6",
      "primary",
      "TEAM06"
    ],
    station12:[
      "floor1-sewing-section2-line7",
      "primary",
      "TEAM07"
    ],
    station13:[
      "floor1-sewing-section2-line8",
      "primary",
      "TEAM08"
    ],
    station14:[
      "floor1-sewing-section2-line9",
      "primary",
      "TEAM09"
    ],
    station15:[
      "floor1-sewing-section2-line6",
      "assembly_point1",
      "TEAM06"
    ],
    station16:[
      "floor1-sewing-section2-line7",
      "assembly_point1",
      "TEAM07"
    ],
    station17:[
      "floor1-sewing-section2-line8",
      "assembly_point1",
      "TEAM08"
    ],
    station18:[
      "floor1-sewing-section2-line9",
      "assembly_point1",
      "TEAM09"
    ]
  };

  at:String = "endline_qc";
  
  btn_classes = {
    station1:"row panel",
    station2:"row panel",
    station3:"row panel",
    station4:"row panel",
    station5:"row panel",
    station6:"row panel",
    station7:"row panel",
    station8:"row panel",
    station9:"row panel",
    station10:"row panel",
    station11:"row panel",
    station12:"row panel",
    station13:"row panel",
    station14:"row panel",
    station15:"row panel",
    station16:"row panel",
    station17:"row panel",
    station18:"row panel",
  }

  constructor(
    private authService:AuthService,
    private dataService:DataService
  ) { }

  ngOnInit() {

    this.dataService.currentDateValue.subscribe(date => this.selected_date = date);

    for (let entry in this.stations) {

      //console.log(this.dataService.getCurrentDate());

      this.authService.getFirstEvent(this.stations[entry][0],"aal-aepz",this.selected_date,this.stations[entry][1],this.at).subscribe(data => {

        if(data.hits.hits.length != 0){
          this.btn_classes[entry] = "green-palatte";
        } else {
          this.btn_classes[entry] = "red-palatte";
        }

      });
      
    }

  }

}
