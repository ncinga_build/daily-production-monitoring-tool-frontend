import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EndlineQcComponent } from './endline-qc.component';

describe('EndlineQcComponent', () => {
  let component: EndlineQcComponent;
  let fixture: ComponentFixture<EndlineQcComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EndlineQcComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EndlineQcComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
