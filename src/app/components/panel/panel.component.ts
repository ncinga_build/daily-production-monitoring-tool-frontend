import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-panel',
  templateUrl: './panel.component.html',
  styleUrls: ['./panel.component.css']
})
export class PanelComponent implements OnInit {
  
  @Input() line :string;
  @Input() hsk :string;
  @Input() dis_name :string;
  @Input() btn_class: string = "row panel";

  constructor() { }

  ngOnInit() {
  }

}
