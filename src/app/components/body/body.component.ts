import { Component, OnInit } from '@angular/core';
import { DataService } from '../../services/data.service';
import { App } from '../../classes/app';
import { APPS } from '../../classes/apps';

@Component({
  selector: 'app-body',
  templateUrl: './body.component.html',
  styleUrls: ['./body.component.css']
})
export class BodyComponent implements OnInit {

  apps = APPS;

  factory: String;

  constructor(
    private dataService:DataService
  ) { }

  ngOnInit() {
    
    this.dataService.currentFactoryValue.subscribe(value => this.factory = value);
    //console.log(this.factory);
  }

}
