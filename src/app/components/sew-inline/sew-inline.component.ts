import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../services/auth.service';
import { DataService } from '../../services/data.service'; 

@Component({
  selector: 'app-sew-inline',
  templateUrl: './sew-inline.component.html',
  styleUrls: ['./sew-inline.component.css']
})
export class SewInlineComponent implements OnInit {

  selected_date:String;

  stations = {
    
    station1:[
      "floor1-sewing-section1-line1-station1",
      "front_qc1",
      "Front-TEAM01"
    ],
    station2:[
      "floor1-sewing-section1-line1-station2",
      "back_qc1",
      "BACK-TEAM01"
    ],
    station3:[
      "floor1-sewing-section1-line1-station3",
      "waist_qc1",
      "Waist-TEAM01"
    ],
    station4:[
      "floor1-sewing-section1-line2-station1",
      "front_qc1",
      "Front-TEAM02"
    ],
    station5:[
      "floor1-sewing-section1-line2-station2",
      "back_qc1",
      "Back-TEAM02"
    ],
    station6:[
      "floor1-sewing-section1-line2-station3",
      "waist_qc1",
      "Waist-TEAM02"
    ],
    station7:[
      "floor1-sewing-section1-line3-station1",
      "front_qc1",
      "Front-TEAM03"
    ],
    station8:[
      "floor1-sewing-section1-line3-station2",
      "back_qc1",
      "Back-TEAM03"
    ],
    station9:[
      "floor1-sewing-section1-line3-station3",
      "waist_qc1",
      "Waist-TEAM03"
    ],
    station10:[
      "floor1-sewing-section1-line4-station1",
      "front_qc1",
      "Front-TEAM04"
    ],
    station11:[
      "floor1-sewing-section1-line4-station2",
      "back_qc1",
      "Back-TEAM04"
    ],
    station12:[
      "floor1-sewing-section1-line4-station3",
      "waist_qc1",
      "Waist-TEAM04"
    ],
    station13:[
      "floor1-sewing-section2-line5-station1",
      "front_qc1",
      "Front-TEAM05"
    ],
    station14:[
      "floor1-sewing-section2-line5-station2",
      "back_qc1",
      "BACK-TEAM05"
    ],
    station15:[
      "floor1-sewing-section2-line5-station3",
      "waist_qc1",
      "Waist-TEAM05"
    ],
    station16:[
      "floor1-sewing-section2-line6-station1",
      "front_qc1",
      "Front-TEAM06"
    ],
    station17:[
      "floor1-sewing-section2-line6-station2",
      "back_qc1",
      "Back-TEAM06"
    ],
    station18:[
      "floor1-sewing-section2-line6-station3",
      "waist_qc1",
      "Waist-TEAM06"
    ],
    station19:[
      "floor1-sewing-section2-line7-station1",
      "front_qc1",
      "Front-TEAM07"
    ],
    station20:[
      "floor1-sewing-section2-line7-station2",
      "back_qc1",
      "Back-TEAM07"
    ],
    station21:[
      "floor1-sewing-section2-line7-station3",
      "waist_qc1",
      "Waist-TEAM07"
    ],
    station22:[
      "floor1-sewing-section2-line8-station1",
      "front_qc1",
      "Front-TEAM08"
    ],
    station23:[
      "floor1-sewing-section2-line8-station2",
      "back_qc1",
      "Back-TEAM08"
    ],
    station24:[
      "floor1-sewing-section2-line8-station3",
      "waist_qc1",
      "Waist-TEAM08"
    ],
    station25:[
      "floor1-sewing-section2-line9-station1",
      "front_qc1",
      "Front-TEAM09"
    ],
    station26:[
      "floor1-sewing-section2-line9-station2",
      "back_qc1",
      "Back-TEAM09"
    ],
    station27:[
      "floor1-sewing-section2-line9-station3",
      "waist_qc1",
      "Waist-TEAM09"
    ]

  };

  btn_classes = {
    station1:"row panel",
    station2:"row panel",
    station3:"row panel",
    station4:"row panel",
    station5:"row panel",
    station6:"row panel",
    station7:"row panel",
    station8:"row panel",
    station9:"row panel",
    station10:"row panel",
    station11:"row panel",
    station12:"row panel",
    station13:"row panel",
    station14:"row panel",
    station15:"row panel",
    station16:"row panel",
    station17:"row panel",
    station18:"row panel",
    station19:"row panel",
    station20:"row panel",
    station21:"row panel",
    station22:"row panel",
    station23:"row panel",
    station24:"row panel",
    station25:"row panel",
    station26:"row panel",
    station27:"row panel"
  }

  at:String = "sew_inline";

  constructor(
    private authService:AuthService,
    private dataService:DataService
  ) { }

  ngOnInit() {

    this.dataService.currentDateValue.subscribe(date => this.selected_date = date);

    for (let entry in this.stations) {

      //console.log(this.dataService.getCurrentDate());

      this.authService.getFirstEvent(this.stations[entry][0],"aal-aepz",this.selected_date,this.stations[entry][1],this.at).subscribe(data => {

        if(data.hits.hits.length != 0){
          this.btn_classes[entry] = "green-palatte";
          // console.log(data.hits.hits);
        } else {
          this.btn_classes[entry] = "red-palatte";
          // console.log("data is not coming.......");
        }
      });
    }

  }

}
