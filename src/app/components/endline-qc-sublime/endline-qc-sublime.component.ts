import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../services/auth.service';
import { DataService } from '../../services/data.service'; 

@Component({
  selector: 'app-endline-qc-sublime',
  templateUrl: './endline-qc-sublime.component.html',
  styleUrls: ['./endline-qc-sublime.component.css']
})
export class EndlineQcSublimeComponent implements OnInit {

  selected_date:String;

  stations = {
    
    station1:[
      "floor3-sewing-line1",
      "primary",
      "K01"
    ],
    station2:[
      "floor3-sewing-line2",
      "primary",
      "K02"
    ],
    station3:[
      "floor3-sewing-line3",
      "primary",
      "K03"
    ],
    station4:[
      "floor3-sewing-line4",
      "primary",
      "K04"
    ],
    station5:[
      "floor3-sewing-line5",
      "primary",
      "K05"
    ],
    station6:[
      "floor3-sewing-line6",
      "primary",
      "K06"
    ],
    station7:[
      "floor3-sewing-line1",
      "secondary",
      "K01"
    ],
    station8:[
      "floor3-sewing-line2",
      "secondary",
      "K02"
    ],
    station9:[
      "floor3-sewing-line3",
      "secondary",
      "K03"
    ],
    station10:[
      "floor3-sewing-line4",
      "secondary",
      "K04"
    ],
    station11:[
      "floor3-sewing-line5",
      "secondary",
      "K05"
    ],
    station12:[
      "floor3-sewing-line6",
      "secondary",
      "K06"
    ],

  }

  at:String = "endline_qc";

  btn_classes = {
    station1:"row panel",
    station2:"row panel",
    station3:"row panel",
    station4:"row panel",
    station5:"row panel",
    station6:"row panel",
    station7:"row panel",
    station8:"row panel",
    station9:"row panel",
    station10:"row panel",
    station11:"row panel",
    station12:"row panel"
  }

  constructor(
    private authService:AuthService,
    private dataService:DataService
  ) { }

  ngOnInit() {

    for (let entry in this.stations) {

      //console.log(this.dataService.getCurrentDate());

      this.dataService.currentDateValue.subscribe(date => this.selected_date = date);

      this.authService.getFirstEvent(this.stations[entry][0],"sublime",this.selected_date,this.stations[entry][1],this.at).subscribe(data => {

        if(data.hits.hits.length != 0){
          this.btn_classes[entry] = "green-palatte";
          // console.log(data.hits.hits);
        } else {
          this.btn_classes[entry] = "red-palatte";
          // console.log("data is not coming.......");
        }
      });
    }

  }

}
