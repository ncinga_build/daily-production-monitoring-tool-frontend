import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FactorySelectionComponent } from './factory-selection.component';

describe('FactorySelectionComponent', () => {
  let component: FactorySelectionComponent;
  let fixture: ComponentFixture<FactorySelectionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FactorySelectionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FactorySelectionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
