import { Component, OnInit } from '@angular/core';
import { LINES } from '../../classes/lines';
import { Line } from '../../classes/line';
import { FACTORIES } from '../../classes/factories';
import { Factory } from '../../classes/factory';

@Component({
  selector: 'app-time-event-search',
  templateUrl: './time-event-search.component.html',
  styleUrls: ['./time-event-search.component.css']
})
export class TimeEventSearchComponent implements OnInit {

  selectedFactory: Factory;
  factories = FACTORIES;

  selectedLine: Line;
  lines = LINES;

  constructor() { }

  ngOnInit() {
    //console.log(this.selected_line);
  }

  onFactorySelect(factory: Factory): void {
    this.selectedFactory = factory;
  }

  onLineSelect(line: Line): void {
    this.selectedLine = line;
  }

}
