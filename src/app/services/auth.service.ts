import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(
    private http:Http
  ) { }

  getFirstEvent(line, factory, date, hsk, at){
    return this.http.get('http://localhost:3000/events/get_first_event?line=' + line + '&factory=' + factory + '&date=' + date + '&hsk=' + hsk + '&at=' + at).pipe(map(res => res.json()));  
  }

  getTimeEvent(factory, date, line){
    return this.http.get('http://localhost:3000/events/get_time_event?line=' + line + '&factory=' + factory + '&date=' + date ).pipe(map(res => res.json()));  
  }

}
